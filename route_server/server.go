package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net"
	"os"
	"sync"
	"time"

	"github.com/rs/zerolog"
	zlog "github.com/rs/zerolog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/examples/data"
	"google.golang.org/protobuf/proto"

	pb "web-service-grpc/routeguide"
)

var (
	tls        = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile   = flag.String("cert_file", "", "The TLS cert file")
	keyFile    = flag.String("key_file", "", "The TLS key file")
	jsonDBFile = flag.String("data", "", "A json file containing a list of features")
	port       = flag.Int("port", 50051, "The server port")
)

type routeGuideServer struct {
	pb.UnimplementedRouteGuideServer
	savedFeatures []*pb.Feature // read-only after initialized

	mu         sync.Mutex // protects routeNotes
	routeNotes map[string][]*pb.RouteNote
}

func newServer() *routeGuideServer {
	s := &routeGuideServer{routeNotes: make(map[string][]*pb.RouteNote)}
	s.loadFeatures(*jsonDBFile)
	return s
}

// Unary call request <> response
// implements grpc from 'UnimplementedRouteGuideServer'
func (s *routeGuideServer) GetFeature(ctx context.Context, point *pb.Point) (*pb.Feature, error) {
	// check if point already exists as feature
	for _, feature := range s.savedFeatures {
		if proto.Equal(feature.Location, point) {
			return feature, nil
		}
	}
	// No feature was found, return an unnamed feature (leaving out 'Name:')
	return &pb.Feature{Location: point}, nil
}

// Server-Side streaming: There can be multiple Features along a certain route
// ListFeatures lists all features contained within the given bounding Rectangle.
func (s *routeGuideServer) ListFeatures(rect *pb.Rectangle, stream pb.RouteGuide_ListFeaturesServer) error {
	for _, feature := range s.savedFeatures {
		// only list feature if it is within the rectangled area of the last two coordinates
		// => is Feature on current route (first point should be current position)
		if inRange(feature.Location, rect) {
			if err := stream.Send(feature); err != nil {
				return err
			}
		}
	}
	return nil
}

// Client-Side streaming: A Route consists of many Points
// RecordRoute receives a stream of points and creates a RouteSummary
func (s *routeGuideServer) RecordRoute(stream pb.RouteGuide_RecordRouteServer) error {
	zlog := zlog.New(os.Stderr).With().Timestamp().Logger()
	zlog = zlog.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	var pointCount, featCount, dist int32
	var lastPoint *pb.Point
	start := time.Now()
	for {
		point, err := stream.Recv()
		// TODO: When/ how does the client signal the close?
		// always catch the default client error for send close
		if err == io.EOF {
			end := time.Now()
			zlog.Info().Str("side", "server").Msg("Client message done.")
			return stream.SendAndClose(&pb.RouteSummary{
				PointCount:   pointCount,
				FeatureCount: featCount,
				Distance:     dist,
				ElapsedTime:  int32(end.Sub(start).Seconds()),
			})
		}
		if err != nil {
			return err
		}
		pointCount++
		// check if point is already in Features, else count as new Feature
		for _, feat := range s.savedFeatures {
			if proto.Equal(point, feat.Location) {
				featCount++
			}
		}
		// calculate new distance
		if lastPoint != nil {
			zlog.Info().Str("side", "server").Msg(fmt.Sprintf("calculate distance: lastPoint(Lat:%d, Lon:%d) point((Lat:%d, Lon:%d))", lastPoint.Latitude, lastPoint.Longitude, point.Latitude, point.Longitude))
			dist += calcDistance(lastPoint, point)
		}
		lastPoint = point
	}
}

// Bi-Directional Stream as a "conversation" on each particular Route Point
// RouteChat receives a stream of message/location pairs, and responds with a stream of all
// previous messages at each of those locations.
// Step 1: Store the new 'RouteNote' on that Loacation
// Step 2: Send back the full conversation for this Location back to the client
func (s *routeGuideServer) RouteChat(stream pb.RouteGuide_RouteChatServer) error {
	zlog := zlog.New(os.Stderr).With().Timestamp().Logger()
	zlog = zlog.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	for {
		in, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		// Location/Point of RouteNote as key
		key := serialize(in.Location)

		// -------------- STEP 1--------------
		// append new RouteNote to previous Notes on that location
		s.mu.Lock()
		s.routeNotes[key] = append(s.routeNotes[key], in)
		// Note: this copy prevents blocking other clients while serving this one.
		// We don't need to do a deep copy, because elements in the slice are
		// insert-only and never modified.
		rn := make([]*pb.RouteNote, len(s.routeNotes[key]))
		copy(rn, s.routeNotes[key])
		s.mu.Unlock()

		// -------------- STEP 2--------------
		// send the new 'comment' along with all previous 'comments' back to the client
		for _, note := range rn {
			zlog.Info().Str("side", "server").Msg(fmt.Sprintf("Loc: Point(%d %d) Msg: %s", note.Location.Latitude, note.Location.Longitude, note.Message))
			if err := stream.Send(note); err != nil {
				return err
			}
		}
	}

}

func main() {
	// TODO: middleware => logging
	zlog := zlog.New(os.Stderr).With().Timestamp().Logger()
	zlog = zlog.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	flag.Parse()

	l, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		zlog.Fatal().Err(err).Msg("Could not establish listener-")
	}
	zlog.Info().Msg(fmt.Sprintf("listen on localhost:%d", *port))

	var opts []grpc.ServerOption
	if *tls {
		if *certFile == "" {
			*certFile = data.Path("x509/server_cert.pem")
		}
		if *keyFile == "" {
			*keyFile = data.Path("x509/server_key.pem")
		}
		creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
		if err != nil {
			zlog.Fatal().Err(err).Msg("Failed to generate credentials")
		}
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}

	// Start: go run ./route_server -port 50058 -data testdata/route_guide_db.json
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterRouteGuideServer(grpcServer, newServer())
	grpcServer.Serve(l)
}

// ----------------------------- helpers----------------------------

// checks if point is within the area of the rectangle
func inRange(point *pb.Point, rect *pb.Rectangle) bool {
	left := math.Min(float64(rect.Lo.Longitude), float64(rect.Hi.Longitude))
	right := math.Max(float64(rect.Lo.Longitude), float64(rect.Hi.Longitude))
	top := math.Max(float64(rect.Lo.Latitude), float64(rect.Hi.Latitude))
	bottom := math.Min(float64(rect.Lo.Latitude), float64(rect.Hi.Latitude))

	if float64(point.Longitude) >= left &&
		float64(point.Longitude) <= right &&
		float64(point.Latitude) >= bottom &&
		float64(point.Latitude) <= top {
		return true
	}
	return false
}

// loadFeatures loads features from a JSON file.
func (s *routeGuideServer) loadFeatures(filePath string) {
	var data []byte
	if filePath != "" {
		var err error
		data, err = ioutil.ReadFile(filePath)
		if err != nil {
			log.Fatalf("Failed to load default features: %v", err)
		}
	}
	if err := json.Unmarshal(data, &s.savedFeatures); err != nil {
		log.Fatalf("Failed to load default features: %v", err)
	}
}

func toRadians(num float64) float64 {
	return num * math.Pi / float64(180)
}

// calcDistance calculates the distance between two points using the "haversine" formula.
// The formula is based on http://mathforum.org/library/drmath/view/51879.html.
func calcDistance(p1 *pb.Point, p2 *pb.Point) int32 {
	const CordFactor float64 = 1e7
	const R = float64(6371000) // earth radius in metres
	lat1 := toRadians(float64(p1.Latitude) / CordFactor)
	lat2 := toRadians(float64(p2.Latitude) / CordFactor)
	lng1 := toRadians(float64(p1.Longitude) / CordFactor)
	lng2 := toRadians(float64(p2.Longitude) / CordFactor)
	dlat := lat2 - lat1
	dlng := lng2 - lng1

	a := math.Sin(dlat/2)*math.Sin(dlat/2) +
		math.Cos(lat1)*math.Cos(lat2)*
			math.Sin(dlng/2)*math.Sin(dlng/2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	distance := R * c
	return int32(distance)
}

func serialize(point *pb.Point) string {
	return fmt.Sprintf("%d %d", point.Latitude, point.Longitude)
}
