module web-service-grpc/server

go 1.18

require (
	github.com/rs/zerolog v1.27.0
	google.golang.org/grpc v1.47.0
	google.golang.org/grpc/examples v0.0.0-20220712183752-9ba66f1b8420
	web-service-grpc/routeguide v0.0.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200806141610-86f49bd18e98 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)

replace web-service-grpc/routeguide v0.0.0 => ../routeguide
