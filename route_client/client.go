package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"

	zlog "github.com/rs/zerolog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "web-service-grpc/routeguide"
)

var (
	port = flag.Int("port", 50051, "the server port")
)

func printFeature(client pb.RouteGuideClient, point *pb.Point, lg zlog.Logger) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer ctxCancel()
	feat, err := client.GetFeature(ctx, point)
	if err != nil {
		lg.Fatal().Err(err).Msg("GetFeature failed")
	}
	lg.Info().Str("side", "client").Msg(fmt.Sprintf("%+v", feat))
}

func printFeaturesInScope(client pb.RouteGuideClient, rect *pb.Rectangle, lg zlog.Logger) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer ctxCancel()
	stream, err := client.ListFeatures(ctx, rect)
	if err != nil {
		lg.Fatal().Err(err).Msg("Could not get client.ListFeatures")
	}
	for {
		feat, err := stream.Recv()
		if err == io.EOF {
			lg.Info().Str("side", "client").Msg("Stream closed.")
			break
		}
		if err != nil {
			lg.Fatal().Err(err).Msg("Stream aborted.")
		}
		lg.Info().Str("side", "client").Msg(fmt.Sprintf("Feature(name: %s location(lon: %d lat: %d))", feat.GetName(), feat.GetLocation().Longitude, feat.Location.GetLatitude()))
	}
}

func printRecordRoute(client pb.RouteGuideClient, lg zlog.Logger) error {
	// Create random points
	r := rand.New(rand.NewSource(time.Now().UnixNano())) // random seed
	countPoints := r.Int31n(50) + 2
	var randPoints []*pb.Point
	for i := 0; i < int(countPoints); i++ {
		randPoints = append(randPoints, randomPoint(r))
	}

	ctx, ctxCancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer ctxCancel()
	stream, err := client.RecordRoute(ctx)
	if err != nil {
		lg.Fatal().Err(err).Msg("Client.RecordRoute aborted.")
	}

	for _, p := range randPoints {
		if err := stream.Send(p); err != nil {
			lg.Fatal().Err(err).Msg(fmt.Sprintf("Failed sream.Send Point(%v)", err))
		}
	}
	recRouteSum, err := stream.CloseAndRecv()
	if err != nil {
		lg.Fatal().Err(err).Msg("Recv failed.")
	}
	lg.Info().Str("side", "client").Msg(fmt.Sprintf("RouteSummary %v", recRouteSum))

	return nil
}

func main() {
	flag.Parse()
	var opts []grpc.DialOption

	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))

	zlogs := zlog.New(os.Stderr).With().Timestamp().Logger()
	zlogs = zlogs.Output(zlog.ConsoleWriter{Out: os.Stderr})

	conn, err := grpc.Dial(fmt.Sprintf("localhost:%d", *port), opts...)
	if err != nil {
		zlogs.Fatal().Err(err).Msg("Failed to generate credentials")
	}
	defer conn.Close()
	client := pb.NewRouteGuideClient(conn)

	// Looking for a valid feature
	printFeature(client, &pb.Point{Latitude: 409146138, Longitude: -746188906}, zlogs)

	// Feature missing.
	printFeature(client, &pb.Point{Latitude: 0, Longitude: 0}, zlogs)

	printFeaturesInScope(client, &pb.Rectangle{
		Lo: &pb.Point{Latitude: 409224445, Longitude: -748286738},
		Hi: &pb.Point{Latitude: 404310607, Longitude: -740282632},
	}, zlogs)

	printRecordRoute(client, zlogs)
}

// --------------------------- helper-----------------------------
func randomPoint(r *rand.Rand) *pb.Point {
	lat := (r.Int31n(180) - 90) * 1e7
	long := (r.Int31n(360) - 180) * 1e7
	return &pb.Point{Latitude: lat, Longitude: long}
}
